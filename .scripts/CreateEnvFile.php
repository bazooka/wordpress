<?php

namespace Bazooka;

class CreateEnvFile {

	public static function init() {
		echo "\n";
		echo "\n";
		echo "📦 Installing WordPress…";
		echo "\n";

		$destination = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;

		CreateEnvFile::createEnvFile( $destination );
		CreateEnvFile::removeScriptFolder( dirname( __DIR__ ) . DIRECTORY_SEPARATOR . '.scripts' );

		echo "\n";
		echo "👌 Installation completed, have fun!";
		echo "\n";
		echo "\n";
	}

	/**
	 * @param string $destination
	 */
	public static function createEnvFile( string $destination ) {
		echo "\n";
		echo "💬 Creating .env file…";
		echo "\n";

		$file   = '.env';
		$handle = fopen( $destination . $file, 'w' );

		echo "\n";
		echo "💬 What's the DB Host?";
		echo "\n";
		$dbHost     = readline();
		echo "\n";
		echo "💬 What's the DB Name?";
		echo "\n";
		$dbName     = readline();
		echo "\n";
		echo "💬 What's the DB User?";
		echo "\n";
		$dbUser     = readline();
		echo "\n";
		echo "💬 What's the DB Password?";
		echo "\n";
		$dbPassword = readline();
		echo "\n";
		echo "💬 What's the site URL?";
		echo "\n";
		$siteUrl    = readline();
		echo "\n";

		$data = "DB_HOST=" . $dbHost;
		$data .= "\n";
		$data .= "DB_NAME=" . $dbName;
		$data .= "\n";
		$data .= "DB_USER=" . $dbUser;
		$data .= "\n";
		$data .= "DB_PASSWORD=" . $dbPassword;
		$data .= "\n";
		$data .= "URL=" . $siteUrl;
		$data .= "\n";

		fwrite( $handle, $data );

		echo "\n";
		echo "👌 .env created!";
		echo "\n";
	}

	/**
	 * @param string $dir
	 */
	public static function removeScriptFolder( string $dir ) {
		foreach ( scandir( $dir ) as $file ) {
			if ( '.' === $file || '..' === $file ) {
				continue;
			}
			if ( is_dir( "$dir/$file" ) ) {
				CreateEnvFile::removeScriptFolder( "$dir/$file" );
			} else {
				unlink( "$dir/$file" );
			}
		}

		rmdir( $dir );
	}
}
